<?php
namespace PMC_U;

use PMC_U\lib\lib_PMC;
use pocketmine\command\CommandExecutor;
use pocketmine\command\PluginCommand;
use pocketmine\event\Listener;

class PMC_U extends lib_PMC implements CommandExecutor, Listener {

	public $lastMem = 0;
	public $maxMem = 0;

	public function onLoad(){
		@mkdir($this->getDataFolder());
		//$this->saveDefaultConfig();
	}

	public function onEnable(){
		//$cfg = $this->getConfig()->getAll();
		$this->getServer()->getScheduler()->scheduleDelayedRepeatingTask(new Tasks\ShowMemoryTask($this), 1, 20 * 10);
		$this->initCommandParams();

		/** @var PluginCommand $ftCmd */
		$ftCmd = $this->getCommand("pmcu");
		$ftCmd->setExecutor(new Commands\PMC_UCommands($this));
	}

}
