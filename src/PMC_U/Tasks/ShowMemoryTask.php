<?php

namespace PMC_U\Tasks;

use PMC_U\PMC_U;
use pocketmine\scheduler\PluginTask;


class ShowMemoryTask extends PluginTask {

	/** @var PMC_U $plugin */
	private $plugin;
	const MB = 1048576;

	public function __construct(PMC_U $Plugin){
		parent::__construct($Plugin);
		$this->plugin = $Plugin;
	}

	public function onRun($tick){
		$cm = memory_get_usage();
		$p = $this->plugin;
		$delta = abs(($p->lastMem - $cm) / max($p->lastMem, 1)) * 100;
		if($delta > 7){
			$p->maxMem = max($p->maxMem, $cm);
			$p->getServer()->getLogger()->info('ПАМЯТЬ: ' . floor($cm / self::MB) . ' mb. MAX: '
				. floor($p->maxMem / self::MB) . ' mb. peak_usage: ' . floor(memory_get_peak_usage() / self::MB) . ' mb.');
			$p->lastMem = $cm;
		}
	}
}
