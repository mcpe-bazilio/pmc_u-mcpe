<?php
/* PMC lib v 1.1*/
namespace PMC_U\lib;

use pocketmine\command\CommandSender;
use pocketmine\item\Item;
use pocketmine\level\Level;
use pocketmine\math\Vector3;
use pocketmine\plugin\PluginBase;
use pocketmine\utils\TextFormat;

class lib_PMC extends PluginBase {

	const ITEM_ID_RESTRICT = 0x1ff;
	const ITEM_DAMAGE_RESTRICT = 0xff;

	public $mainCommand;

	/** @var array $subCommands Массив подкоманд */
	public $subCommands = null;

	/** @var array $subCommands Массив алиасов */
	public $subCommandAliases = null;

	/** @var array $itemNames */
	public $itemNames = [
		0   => 'воздух',
		1   => 'камень',
		2   => 'трава',
		3   => 'земля',
		4   => 'булыжник',
		5   => 'доски',
		7   => 'бедрок',
		8   => 'вода',
		9   => 'стоячая вода',
		10  => 'лава',
		12  => 'песок',
		13  => 'гравий',
		14  => 'золотая руда',
		15  => 'железная руда',
		17  => 'дерево',
		18  => 'листья',
		19  => 'губка',
		20  => 'стекло',
		24  => 'песчаник',
		27  => 'энергорельсы',
		28  => 'рельсы с датчиком',
		30  => 'паутина',
		32  => 'мертвый куст',
		35  => 'шерсть',
		37  => 'одуванчик',
		38  => 'мак',
		46  => 'динамит',
		47  => 'книжная полка',
		48  => 'замшелый булыжник',
		49  => 'обсидиан',
		50  => 'факел',
		51  => 'огонь',
		52  => 'рассадник монстров',
		53  => 'дубовые ступеньки',
		54  => 'сундук',
		56  => 'алмазная руда',
		58  => 'верстак',
		60  => 'пашня',
		61  => 'печь',
		65  => 'лестница',
		66  => 'рельсы',
		67  => 'булыжные ступеньки',
		69  => 'рычаг',
		70  => 'каменная нажимная плита',
		72  => 'деревянная нажимная плита',
		73  => 'редолитовая руда',
		75  => 'редолитовый факел',
		78  => 'снег',
		79  => 'лёд',
		81  => 'кактус',
		86  => 'тыква',
		87  => 'незерит',
		88  => 'песок душ',
		89  => 'светящийся камень',
		95  => 'прозрачный блок',
		98  => 'каменные кирпичи',
		101 => 'железные прутья',
		102 => 'стеклянная панель',
		104 => 'стебель тыквы',
		106 => 'лоза',
		108 => 'кирпичные ступеньки',
		109 => 'ступеньки из каменного кирпича',
		110 => 'мицелий',
		111 => 'кувшинка',
		113 => 'незеритовый забор',
		116 => 'стол зачарований',
		120 => 'портал в энд',
		121 => 'эндерняк',
		122 => 'редстоуновая лампа',
		126 => 'активирующие рельсы',
		128 => 'песчаниковые ступеньки',
		129 => 'изумрудная руда',
		131 => 'крюк',
		132 => 'растяжка',
		134 => 'еловые ступеньки',
		135 => 'берёзовые ступеньки',
		136 => 'ступеньки из тропического дерева',
		139 => 'каменная ограда',
		145 => 'наковальня',
		146 => 'сундук-ловушка',
		151 => 'датчик дневного света',
		156 => 'кварцевые ступеньки',
		158 => 'деревянная плита',
		159 => 'окрашенная глина',
		163 => 'акациевые ступеньки',
		164 => 'ступеньки из тёмного дуба',
		167 => 'железный люк',
		170 => 'сноп сена',
		171 => 'ковёр',
		172 => 'обожжёная глина',
		174 => 'плотный лёд',
		198 => 'тропа',
		243 => 'подзол',
		256 => 'железная лопата',
		257 => 'железная кирка',
		258 => 'железный топор',
		259 => 'огниво',
		260 => 'яблоко',
		261 => 'лук',
		262 => 'стрела',
		263 => 'уголь',
		264 => 'алмаз',
		265 => 'железный слиток',
		266 => 'золотой слиток',
		267 => 'железный меч',
		268 => 'деревянный меч',
		269 => 'деревянная лопата',
		270 => 'деревянная кирка',
		271 => 'деревянный топор',
		272 => 'каменный меч',
		273 => 'каменная лопата',
		274 => 'каменная кирка',
		275 => 'каменный топор',
		276 => 'алмазный меч',
		277 => 'алмазная лопата',
		278 => 'алмазная кирка',
		279 => 'алмазный топор',
		280 => 'палка',
		281 => 'миска',
		282 => 'тушеные грибы',
		283 => 'золотой меч',
		284 => 'золотая лопата',
		285 => 'золотая кирка',
		286 => 'золотой топор',
		287 => 'нить',
		288 => 'перо',
		289 => 'порох',
		290 => 'деревянная мотыга',
		291 => 'каменная мотыга',
		292 => 'железная мотыга',
		293 => 'алмазная мотыга',
		294 => 'золотая мотыга',
		295 => 'семена пшеницы',
		296 => 'пшеница',
		297 => 'хлеб',
		298 => 'кожаный шлем',
		299 => 'кожаная куртка',
		300 => 'кожаные штаны',
		301 => 'кожаные ботинки',
		302 => 'койф',
		303 => 'кольчуга',
		304 => 'кольчужные поножи',
		305 => 'кольчужные ботинки',
		306 => 'железный шлем',
		307 => 'железный нагрудник',
		308 => 'железные поножи',
		309 => 'железные ботинки',
		310 => 'алмазный шлем',
		311 => 'алмазный нагрудник',
		312 => 'алмазные поножи',
		313 => 'алмазные ботинки',
		318 => 'кремень',
		319 => 'сырая свинина',
		320 => 'жареная свинина',
		321 => 'картина',
		322 => 'золотое яблоко',
		323 => 'табличка',
		324 => 'деревянная дверь',
		325 => 'ведро',
		328 => 'вагонетка',
		329 => 'седло',
		330 => 'железная дверь',
		331 => 'редолитовая пыль',
		332 => 'снежок',
		334 => 'кожа',
		336 => 'кирпичи',
		337 => 'глина',
		338 => 'сахарный тростник',
		339 => 'бумага',
		340 => 'книга',
		341 => 'сгусток слизи',
		344 => 'яйцо',
		345 => 'компас',
		346 => 'удочка',
		347 => 'часы',
		348 => 'светящаяся пыль',
		349 => 'сырая рыба',
		350 => 'жареная рыба',
		352 => 'кость',
		353 => 'сахар',
		354 => 'торт',
		355 => 'кровать',
		357 => 'печенье',
		359 => 'ножницы',
		360 => 'арбуз',
		361 => 'семена тыквы',
		362 => 'семена арбуза',
		363 => 'сырая говядина',
		364 => 'стейк',
		365 => 'курятина',
		366 => 'жареная курица',
		367 => 'гнилая плоть',
		369 => 'пылающий жезл',
		370 => 'слеза гаста',
		371 => 'золотой самородок',
		372 => 'нарост из незера',
		373 => 'зелье',
		374 => 'пузырёк',
		375 => 'паучий глаз',
		376 => 'маринованный паучий глаз',
		377 => 'огненный порошок',
		378 => 'сгусток магмы',
		379 => 'зельеварка',
		382 => 'сверкающий арбуз',
		388 => 'изумруд',
		390 => 'цветочный горшок',
		391 => 'морковь',
		392 => 'картофель',
		393 => 'печёный картофель',
		394 => 'ядовитый картофель',
		396 => 'золотая морковь',
		400 => 'тыквенный пирог',
		403 => 'зачарованная книга',
		405 => 'незеритовый кирпич',
		406 => 'незер-кварцевая руда',
		411 => 'сырая крольчатина',
		412 => 'жареная крольчатина',
		413 => 'тушёный кролик',
		415 => 'кроличья шкурка',
		457 => 'свёкла',
		458 => 'семена свёклы',
		459 => 'свекольный суп'
	];

	/**
	 * Имя предмета по id:damage. Русское (если найдено) иначе - английское
	 *
	 * @param string $idNdamage
	 *
	 * @return mixed|string
	 */
	public function getItemNameRU(&$idNdamage){
		if(!preg_match('/(\d+)(?::(\d+))*/m', $idNdamage, $matches)) return '';
		$id = $idNdamage = intval($matches[1]) & self::ITEM_ID_RESTRICT;
		$damage = isset($matches[2]) ? intval($matches[2]) & self::ITEM_DAMAGE_RESTRICT : 0;
		if($damage) $idNdamage = $id . ':' . $damage;
		if(isset($this->itemNames[$id])) return $this->itemNames[$id];
		return Item::get($id, $damage)->getName();
	}

	public function initCommandParams(){
		$this->mainCommand = '';
		$this->subCommands = [];
		$this->subCommandAliases = [];
		foreach($this->getDescription()->getCommands() as $CommandName => $CmdInfo){
			$this->mainCommand = strtolower($CommandName);
			foreach($CmdInfo['subCommands'] as $subCmdName => $subCmdInfo){
				$this->subCommands[strtolower($subCmdName)] = $subCmdInfo;
				if(isset($subCmdInfo['aliases'])){
					foreach($subCmdInfo['aliases'] as $alias){
						$this->subCommandAliases[strtolower($alias)] = $subCmdName;
					}
				}
			}
			break; //обрабатываем только первую команду
		}
	}


	public function getMainCommand(){
		if($this->mainCommand == null) $this->initCommandParams();
		return $this->mainCommand;
	}

	public function getSubCommands(){
		if($this->subCommands == null) $this->initCommandParams();
		return $this->subCommands;
	}

	public function getSubCommandAliases(){
		if($this->subCommandAliases == null) $this->initCommandParams();
		return $this->subCommandAliases;
	}


	/**
	 * Убирает пустые аргументы
	 *
	 * @param array $args Аргументы команды
	 *
	 * @return array
	 */
	public function normalizeCommandArgs(&$args){
		$args2 = [];
		foreach($args as $key => $arg){
			if(trim($arg) != '') $args2[] = $arg;
		}
		$args = $args2;
		return $args2;
	}

	public function MSG(CommandSender $s, $ResultCode, $msg, $success = true){
		$color = $this->getColorByResultCode($ResultCode);
		$s->sendMessage($color . $msg);
		return $success;
	}

	static function colorAlias(&$usage, $alias, $aliasColor = '§b', $usageColor = '§e'){
		if(!preg_match('%^([a-z0-9§]*/[a-z_0-9]+\s+)([a-z0-9§]+)(.*)$%im', $usage, $matches)) return false;
		$subCmdName = $result = preg_replace('/§\w/i', '', $matches[2]);
		if(preg_match('/^(\w*?)(' . $alias . ')(\w*)$/im', $subCmdName, $m)){
			$subCmdName = $m[1] . $aliasColor . $m[2] . $usageColor . $m[3];
			$usage = $usageColor . $matches[1] . $subCmdName . $matches[3];
			return true;
		}

		$usageNew = $usageColor . $matches[1];
		$residue = $matches[3];
		$aliasLetters = str_split($alias);

		foreach($aliasLetters as $letter){
			if(!preg_match('/^(\w*?)' . $letter . '(\w*)/i', $subCmdName, $matches)) return false;
			$usageNew .= $matches[1] . $aliasColor . $letter . $usageColor;
			$subCmdName = $matches[2];
		}
		$usage = $usageNew . $matches[2] . $residue;
		return true;
	}

	static function getColorByResultCode($ResultCode){
		switch($ResultCode){
			case 0:
				return TextFormat::RED;
			case 1:
				return TextFormat::GREEN;
			case 2:
				return TextFormat::AQUA;
			case 3:
				return TextFormat::YELLOW;
		}
		return TextFormat::AQUA;
	}


	public function getCmdUsage($subCmdName, $ResultCode, $bLabel = false, $bAlias = true){
		$subCmdName = strtolower($subCmdName);
		$mainCommand = $this->getMainCommand();
		$usageColor = '§e';
		$prefix = ($bLabel ? 'Используйте: ' : '') . $usageColor;
		$aliasColor = '§b';
		$aliasSuffix = '';
		$endColor = $this->getColorByResultCode($ResultCode);

		if($mainCommand == $subCmdName){
			$usage = $this->getDescription()->getCommands()[$subCmdName]["usage"];
		}else{
			if(!isset($this->getSubCommands()[$subCmdName])) return '';
			$sc = $this->getSubCommands()[$subCmdName];
			if(!isset($sc["usage"])) return '';
			$usage = $sc["usage"];

			if($bAlias){
				if(isset($sc['aliases']) && isset($sc['aliases'][0])){
					$alias = $sc['aliases'][0];
					if(!$this->colorAlias($usage, $alias, $aliasColor, $usageColor)){
						$aliasSuffix = '§f Алиас:' . $usageColor . ' /' . $mainCommand . $aliasColor . ' ' . $alias;
					}
				}
			}
		}
		return $prefix . $usage . $aliasSuffix . $endColor;
	}

	public function printHelp(CommandSender $s, $bMainCmdUsage = false, $success = true){
		$hlp = "§f======§d  " . $this->getMainCommand() . " help  §f====== §1(плагин разработан специально для §5PLAY-MC.RU§1) §f======";
		if($bMainCmdUsage){
			$mainCmd = $this->getDescription()->getCommands()[$this->mainCommand];
			$usage = isset($mainCmd["usage"]) ? $mainCmd["usage"] : '';
			$descr = isset($mainCmd["description"]) ? $mainCmd["description"] : '';
			$hlp .= "\n§e" . $usage . '§a - ' . $descr;
		}
		foreach($this->getSubCommands() as $subCmdName => $sc){
			if(isset($sc["no-help"]) && $sc["no-help"]) continue;
			$usage = $this->getCmdUsage($subCmdName, 3);
			$descr = isset($sc["description"]) ? $sc["description"] : '';
			$hlp .= "\n§e" . $usage . '§a - ' . $descr;
		}
		$hlp .= "\n§7Выделенные§b буквы§7 в командах можно использовать как алиасы. Например, §e/cmd §bsc§7 вместо §e/cmd §bs§eub§bc§emd §7";
		$this->MSG($s, 3, $hlp);
		return $success;
	}

	function canUseSubCommand(CommandSender $s, $subCmd, $subCmdOrig){
		$subCmdLC = strtolower($subCmd);
		if(!$s->hasPermission($this->getMainCommand() . ".command." . $subCmdLC)){
			$this->MSG($s, 0, "[✘] У Вас нет прав на выполнение команды " . $this->frc("/" . $this->getMainCommand() . " $subCmdOrig", 0));
			return false;
		}
		return true;
	}

	/**
	 * @param CommandSender|null $s
	 * @param string             $subCmd
	 * @param bool               $bNoAlert
	 *
	 * @return bool
	 */
	function checkSubCommand($s, &$subCmd, $bNoAlert = false){
		$subCmdLC = strtolower($subCmd);
		if(!array_key_exists($subCmdLC, $this->getSubCommands())){
			if(!array_key_exists($subCmdLC, $this->getSubCommandAliases())){
				if($bNoAlert && $s !== null){
					$msg = "[✘] Неизвестная подкоманда " . $this->ec($subCmd, 0) . " " . $this->getCmdUsage('help', 0, true) . " для справки.";
					$this->MSG($s, 0, $msg);
				}
				return false;
			}
			$subCmd = $this->getSubCommandAliases()[$subCmdLC];
		}
		return true;
	}

	function validateNameAgainstSubCommands($name){
		$nameLC = strtolower($name);
		if(!$this->checkSubCommand(null, $nameLC, true)) return true;
		$arr = array_merge(array_keys($this->getSubCommands()), array_keys($this->getSubCommandAliases()));
		$reservedNamesList = implode(', ', $arr);
		return $reservedNamesList;
	}

	public function compactCoord($coord, $decimals = 1){
		return $this->s_rtrim(number_format(floatval($coord), $decimals), '.0');
	}

	public function s_rtrim($str, $toCut){
		$pattern = preg_replace('/\./', '\.', $toCut);
		return preg_replace('~' . $pattern . '$~i', '', $str);
	}

	/**
	 * Форматирует координаты стандартным образом
	 * Либо первые 3 параметра - x,y,z, либо первый параметр массив кординат или объект, содержащий методы getX...
	 * за ними - флаг "выводить лейбл координаты" (по ум. false) и число знаков после запятой в координатах (по ум. 0)
	 *
	 * @param array $args
	 *
	 * @return string
	 */
	public function fCrd(...$args){
		$ar = ["string", "integer", "double"];
		$i = 2;
		if(count($args) > 2 && in_array(gettype($args[0]), $ar) && in_array(gettype($args[1]), $ar) && in_array(gettype($args[2]), $ar)){
			$x = floatval($args[0]);
			$y = floatval($args[1]);
			$z = floatval($args[2]);
		}elseif(count($args) > 0){
			if(gettype($args[0]) == 'object' && method_exists($args[0], 'getX')){
				$x = $args[0]->getX();
				$y = $args[0]->getY();
				$z = $args[0]->getZ();
			}elseif(gettype($args[0]) == 'array' && in_array(gettype($args[0][0]), $ar) && in_array(gettype($args[0][1]), $ar) && in_array(gettype($args[0][2]), $ar)){
				$x = floatval($args[0][0]);
				$y = floatval($args[0][1]);
				$z = floatval($args[0][2]);
			}else{
				return '';
			}
			$i = 0;
		}else{
			return '';
		}
		$label = (isset($args[$i + 1])) ? !!$args[$i + 1] : false;
		$dec = (isset($args[$i + 2])) ? min(max(0, intval($args[$i + 2])), 3) : 0;

		$x = $this->compactCoord($x, $dec);
		$y = $this->compactCoord($y, $dec);
		$z = $this->compactCoord($z, $dec);
		$ret = ($label ? '§dКоординаты ' : '');
		return $ret . "§7x:§6$x §7y:§6$y §7z:§6$z";
	}

	/**
	 * Выделяет цветом участок текста (за выделенным будет цвет в соответствии с $ResultCode)
	 *
	 * @param string $txt   - выделяемый текст
	 * @param int    $ResultCode
	 * @param string $color цвет выделения в формате '§x' . По умолчанию: LIGHT_PURPLE
	 *
	 * @return string
	 */
	public function ec($txt, $ResultCode, $color = null){
		if($color == null) $color = '§d';
		$nextColor = $this->getColorByResultCode($ResultCode);
		return $color . $txt . $nextColor;
	}

	/**
	 * Форматирует команду стандартным образоm
	 *
	 * @param string     $cmd
	 * @param string|int $nextColor
	 *
	 * @return string
	 */
	public function frc($cmd, $nextColor){
		if(is_integer($nextColor)){
			$nextColor = $this->getColorByResultCode($nextColor);
		}
		return '§e' . $cmd . $nextColor . '';
	}


	/**
	 * Форматирует имя мира стандартным образом
	 *
	 * @param string     $levelName
	 * @param string|int $nextColor
	 * @param string     $q
	 *
	 * @return string
	 */
	public function frw($levelName, $nextColor, $q = ''){
		if(is_integer($nextColor)){
			$nextColor = $this->getColorByResultCode($nextColor);
		}
		return $nextColor . $q . '§6' . $levelName . $nextColor . $q;
	}

	/**
	 * Форматирует идентификатор сущности образом
	 *
	 * @param string     $id
	 * @param string|int $nextColor
	 * @param string     $q
	 *
	 * @return string
	 */
	public function fri($id, $nextColor, $q = ''){
		if(is_integer($nextColor)){
			$nextColor = $this->getColorByResultCode($nextColor);
		}
		return $nextColor . $q . '§6' . $id . $nextColor . $q;
	}

	/**
	 * Форматирует имя чего-либо  сущности образом
	 *
	 * @param string     $Name
	 * @param string|int $nextColor
	 *
	 * @return string
	 */
	public function frn($Name, $nextColor){
		if(is_integer($nextColor)){
			$nextColor = $this->getColorByResultCode($nextColor);
		}
		return '"§6' . $Name . $nextColor . '"';
	}

	/**
	 * @param  string|array|Vector3 $coords     Координаты в виде строки "x,y,z", массива [x, y, z] или Vector3
	 * @param int                   $returnType Флаг: Возвращать строку (0) массив (1) или Vector3 (2)
	 *
	 * @return array|string|Vector3|bool Нормализованные координаты (1 десятичный знак и без него, для целых) в нужном представлении: строка / массив /  Vector3
	 */
	public function compactCoords($coords, $returnType = 0){
		if($coords instanceof Vector3){
			$c = [$coords->getX(), $coords->getY(), $coords->getZ()];
		}elseif(is_array($coords)){
			$c = $coords;
		}else{
			$c = explode(',', $coords);
		}
		if(!isset($c[0])) $c[0] = 0;
		if(!isset($c[1])) $c[1] = 0;
		if(!isset($c[2])) $c[2] = 0;
		$x = $this->compactCoord($c[0]);
		$y = $this->compactCoord($c[1]);
		$z = $this->compactCoord($c[2]);
		switch($returnType){
			case 2:
				return new Vector3($x, $y, $z);
			case 1:
				return [$x, $y, $z];
			default:
				return "$x,$y,$z";
		}
	}

	/**
	 * @param array|Vector3 $coords Координаты в виде массива [x, y, z] или объект Vector3
	 *
	 * @return string Нормализованные координаты (1 десятичный знак и без него, для целых)
	 */
	public function packCoords($coords){
		return $this->compactCoords($coords, 0);
	}


	/**
	 * Проверяет свободное пространство вокруг заданной точки. (для телепортации)
	 *
	 * @param Vector3 $v3
	 * @param Level   $level
	 *
	 * @return bool
	 */
	public function enoughFreeSpace($v3, $level){
		$minX = $v3->getFloorX() - 1;
		$minY = $v3->getFloorY();
		$minZ = $v3->getFloorZ() - 1;
		$ar = [0, 1, 2];
		$noAirCount = 0;
		foreach($ar as $dx){
			foreach($ar as $dy){
				foreach($ar as $dz){
					$id = $level->getBlockIdAt($minX + $dx, $minY + $dy, $minZ + $dz);
					if($id != 0) $noAirCount++;
					if($noAirCount > 2) return false;
				}
			}
		}
		return ($noAirCount < 3);
	}

	public function mb_str_pad($input, $pad_length, $pad_string, $pad_style, $encoding = "UTF-8"){
		return str_pad($input, strlen($input) - mb_strlen($input, $encoding) + $pad_length, $pad_string, $pad_style);
	}
}
