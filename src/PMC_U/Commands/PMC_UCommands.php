<?php

namespace PMC_U\Commands;

use PMC_U\PMC_U;
use pocketmine\block\Block;
use pocketmine\command\Command;
use pocketmine\command\CommandExecutor;
use pocketmine\command\CommandSender;
use pocketmine\item\Item;
use pocketmine\item\ItemBlock;
use pocketmine\level\Position;
use pocketmine\math\Vector3;
use pocketmine\Player;
use pocketmine\utils\Config;


class PMC_UCommands implements CommandExecutor {

	/** @var PMC_U $plugin */
	public $plugin;

	public function __construct(PMC_U $Plugin){
		$this->plugin = $Plugin;
		//$this->cmdCollectItemsNames();
	}

	/**
	 * @param CommandSender $s
	 * @param Command       $cmd
	 * @param string        $label
	 * @param array         $args
	 *
	 * @return bool
	 */
	public function onCommand(CommandSender $s, Command $cmd, $label, array $args){
		$p = $this->plugin;
		if($cmd->getName() != "pmcu") return false;
		if(!$s->hasPermission("pmcu")) return $p->MSG($s, 0, "[✘] У Вас нет прав для выполнения команды §e/" . $cmd->getName());
		$p->normalizeCommandArgs($args);//Убираем пустые ргументы
		if(!isset($args[0])) return $p->printHelp($s);

		$subCmd = $subCmdOrig = strtolower($args[0]);
		if(!$p->checkSubCommand($s, $subCmd, false)) return $p->MSG($s, 0, "[✘] Неизвестная подкоманда §e" . $subCmdOrig . '. §c' . $p->printHelp($s));

		if(!$p->canUseSubCommand($s, $subCmd, $subCmdOrig)) return true;

		switch($subCmd){
			case "help":
				return $p->printHelp($s);

			case "collectitemsnames":
				return $this->cmdCollectItemsNames($s);

		}
		return false;
	}

	function compactDamages($arIN){
		//Находим групповые повторения
		$arTmp0 = $arTmp = $arIN;
		//Минимальное количество пунктов для сравнения - 3
		$cnt0 = count($arTmp0) - 3;
		$bOk = false; //Флаг "Сравнение найдено"
		for($i = 0; $i < $cnt0; $i++){
			//Откусываем по элементу сначала временного массива и сравниваем остаток с оригинальным массивом (с начала ориг массива)
			//Если совпадение найдено, то откусанное - период повторения
			array_shift($arTmp);
			$bOk = count(array_intersect_assoc($arTmp0, $arTmp)) == count($arTmp);
			if($bOk) break;
		}
		$packedArray = $bOk ? array_slice($arIN, 0, $i + 1, true) : $arIN;
		unset($arTmp0);
		unset($arTmp);
		unset($cnt0);
		unset($i);
		unset($bOk);

		//Убираем повторения в конце

		//обратная сортировка по ключам
		krsort($packedArray, SORT_NUMERIC);
		$arTmp = [];
		foreach($packedArray as $damage => $name){
			$arTmp[] = ['d' => $damage, 'n' => $name];
		}
		if(count($arTmp) > 1){
			$n = $arTmp[0]['n'];
			$cnt = count($arTmp);
			for($i = 1; $i < $cnt; $i++){
				if($arTmp[$i]['n'] != $n){
					break;
				}
				unset($arTmp[$i - 1]);
			}
		}
		$resArr = [];
		foreach($arTmp as $Tmp){
			$resArr[$Tmp['d']] = $Tmp['n'];
		}
		ksort($resArr);
		//if(count($resArr) <= 2) return $resArr;
		return $resArr;
	}


	/**
	 * @param Player|null $receiver
	 *
	 * @return bool
	 */
	private function cmdCollectItemsNames($receiver = null){
		$p = $this->plugin;

		//Рисуем блоки в мире и считываем их следом
		$ar0 = [];
		for($id = 0; $id < 511; $id++){
			/* @var $item Item|ItemBlock */
			for($damage = 0; $damage < 64; $damage++){
				$item = Item::get($id, $damage, 1);
				if($item == null) continue;
				$ar0[$item->getId()][$item->getDamage()] = $item->getName();
			}
			$len1 = count($ar0[$id]);
			if($len1 > 1){
				$ar0[$id] = $this->compactDamages($ar0[$id]);
			}
			//Убираем элемены с единственным дамаджем = 'Unknown'
			if(count($ar0[$id]) == 1 && $ar0[$id][0] == 'Unknown'){
				unset($ar0[$id]);
			}
		}
		unset($id);
		unset($damage);
		unset($item);
		unset($len1);

		$level = $this->plugin->getServer()->getLevelByName('world');
		$dx = 1000;
		$dz = 1000;
		//Рисуем блоки в мире и считываем их следом
		$arDiffReal = [];
		foreach($ar0 as $itemId => $damages){
			if($itemId > 255) continue;
			foreach($damages as $damage => $name){
				$x = $itemId + $dx;
				$z = $damage + $dz;
				$level->setBlock(new Position($x, 90, $z, $level), new Block($itemId, $damage), false, false);
				$block = $level->getBlock(new Vector3($x, 90, $z), false);
				if($itemId != $block->getId() || $damage != $block->getDamage() || $name != $block->getName()){
					$arDiffReal[$itemId][$damage] = [
						'id'     => $block->getId(),
						'damage' => $block->getDamage(),
						'name'   => $block->getName()
					];
					if($damage != $block->getDamage()){
						$arDiffReal[$itemId][$damage]['damage0'] = $damage;
					}
					if($name != $block->getName()){
						$arDiffReal[$itemId][$damage]['name0'] = $name;
					}
				}
				unset($x);
				unset($z);
				unset($block);
			}
		}

		unset($level);
		unset($dx);
		unset($dz);
		unset($itemId);
		unset($damages);
		unset($damage);
		unset($name);

		//отбрасываем имена Unknown и пустые
		foreach($ar0 as $itemId => $damages){
			foreach($damages as $damage => $name){
				if($name == 'Unknown' || trim($name) == ''){
					unset($ar0[$itemId][$damage]);
				}
			}
		}
		unset($itemId);
		unset($damages);
		unset($damage);
		unset($name);


		//Загружем стартовый массив и массив переведенных фраз
		$startItemNames = [];


		$ItemNamesCfg = new Config($p->getDataFolder() . "ItemNames.yml", Config::YAML, []);
		$inTmp = $ItemNamesCfg->getAll();
		if(isset($inTmp['data']) && is_array($inTmp['data'])){
			$startItemNames = $inTmp['data'];
		}
		unset($ItemNamesCfg);
		unset($inTmp);


		$TransCfg = new Config($p->getDataFolder() . "Translations.yml", Config::YAML, []);
		$arTrans = $TransCfg->getAll();
		unset($TransCfg);


		function nt($str){
			$str = trim(mb_strtolower($str));
			$str = preg_replace('/ +/im', ' ', $str);
			return $str;
		}

		function normalizeTranslationArray(&$ar){
			foreach($ar as $EN => $RU){
				$strEN = nt($EN);
				if($EN !== $strEN) unset($ar[$EN]);
				$ar[$strEN] = nt($RU);
			}
			ksort($ar);
		}

		function getENRU($ar, &$EN, &$RU){
			reset($ar);
			$EN = $RU = null;
			while(list($key, $val) = each($ar)){
				if(is_string($val)){
					$EN = $key;
					$RU = $val;
					break;
				}
			}
			return $RU !== null;
		}

		function updateArTrans($arTrans, $ar){
			$EN = $RU = null;
			if(getENRU($ar, $EN, $RU)){
				if(trim($EN) != '' && !isset($arTrans[trim($EN)]) && trim($RU) != ''){
					$arTrans[trim($EN)] = trim($RU);
				}
			}
		}

		//Добавляем переводы из стартового массива
		foreach($startItemNames as $itemID => $info){
			updateArTrans($arTrans, $info);
			if(isset($info['damages'])){
				foreach($info['damages'] as $damage => $EN_RU){
					updateArTrans($arTrans, $EN_RU);
				}
			}
		}
		unset($itemID);
		unset($info);
		unset($damage);
		unset($EN_RU);

		normalizeTranslationArray($arTrans);

		$resultArray = [];
		foreach($ar0 as $itemID => $damages){
			$itemID = intval($itemID);
			$defaultENkey = nt($damages[0]);
			$CI = &$resultArray[$itemID];
			if(isset($arTrans[$defaultENkey]) && trim($arTrans[$defaultENkey]) != ''){
				$CI[$defaultENkey] = trim($arTrans[$defaultENkey]);
			}else{
				$CI[$defaultENkey] = $arTrans[$defaultENkey] = '';
			}
			if(count($damages) < 2) continue;
			$resultArray[$itemID]['damages'] = [];
			$CID = &$resultArray[$itemID]['damages'];
			foreach($damages as $damage => $enKey){
				$enKey = nt($enKey);
				if(isset($arTrans[$enKey]) && trim($arTrans[$enKey]) != ''){
					$CID[$damage][$enKey] = trim($arTrans[$enKey]);
				}else{
					$CID[$damage][$enKey] = $arTrans[$enKey] = '';
				}
			}

		}
		unset($itemID);
		unset($damages);
		unset($defaultENkey);
		unset($CI);
		unset($damage);
		unset($enKey);
		unset($CID);


		//Добавляем из стартового в результирующий массив отсутствующие в последнем записи
		foreach($startItemNames as $itemID => $info){
			if(!isset($resultArray[$itemID])){
				$resultArray[$itemID] = $info;
			}else{
				if(isset($info['damages'])){
					foreach($info['damages'] as $damage => $EN_RU){
						if(!$resultArray[$itemID]['damages']){
							$resultArray[$itemID]['damages'] = [];
						}
						if(!$resultArray[$itemID]['damages'][$damage]){
							$resultArray[$itemID]['damages'][$damage] = $EN_RU;
						}
					}
				}
			}
		}
		unset($itemID);
		unset($info);
		unset($damage);
		unset($EN_RU);

		$array2Translate = [];
		$translated = [];
		$str2Translate = '';
		ksort($arTrans);
		foreach($arTrans as $keyEN => $RU){
			if(trim($RU) == ''){
				$array2Translate[] = $keyEN;
				$str2Translate .= "\n" . $keyEN;
			}else{
				$translated[$keyEN] = $RU;
			}
		}
		unset($keyEN);
		unset($RU);
		unset($arDiffReal);
		sort($array2Translate);

		$toConfig = [
			'server'  => $p->getServer()->getName(),
			'version' => $p->getServer()->getVersion(),
			'data'    => $resultArray
		];
		$ItemsNamesFile = "ItemsNames_" . date('ymd') . ".yml";
		$toTranslateFile = "2Translate_" . date('ymd') . ".yml";
		$translatedFile = "Translated_" . date('ymd') . ".yml";
		new Config($p->getDataFolder() . $ItemsNamesFile, Config::YAML, $toConfig);
		new Config($p->getDataFolder() . $toTranslateFile, Config::YAML, $array2Translate);
		new Config($p->getDataFolder() . $translatedFile, Config::YAML, $translated);


		$msg = "[✔] Файл-массив сущностей сохранен в  " . $p->getDataFolder() . $ItemsNamesFile;
		$msg .= "\nФайл-массив переводов сохранен в  " . $p->getDataFolder() . $translatedFile;
		$msg .= "\nФайл-массив для перевода сохранен в  " . $p->getDataFolder() . $toTranslateFile;
		if($receiver !== null && $receiver instanceof Player){
			return $p->MSG($receiver, 1, $msg);
		}
		$p->getServer()->getLogger()->info($msg);
		return true;
	}
}
